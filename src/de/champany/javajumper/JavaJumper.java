package de.champany.javajumper;

import java.awt.Container;
import java.awt.Dimension;
import de.champany.lib.Keyboard;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class JavaJumper extends JApplet {
	public enum screenType {
		applet, window
	}

	private static final long serialVersionUID = 1L;

	// informations for the title of the JFrame
	public static final String VERSION = "0.8.0a";
	public static final int BUILDDATE = 20151007;
	public static final String NAME = "JavaJumper";
	public static final boolean TESTVERSION = true;

	public static final String CREATOR = "champany.de";
	public static GameManager panel;
	public Keyboard keyboard;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new JavaJumper(screenType.window);
			}
		});
	}

	public JavaJumper() {
		this.init(screenType.applet);
	}

	public JavaJumper(screenType screen) {
		this.init(screen);

	}

	public void init(screenType screen) {
		keyboard = new Keyboard();
		Container content = null;
		JFrame window = null;

		if (screen == screenType.window) {

			window = new JFrame();
			String title = String.format("%s%s by [%s] - %s @%d",
					JavaJumper.TESTVERSION?"[TEST] ":"",JavaJumper.NAME, JavaJumper.CREATOR, JavaJumper.VERSION,
					JavaJumper.BUILDDATE);
			window.setTitle(title);
			window.setResizable(true);
			window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			content = window.getContentPane();
			content.setPreferredSize(new Dimension(800, 600));

		} else if (screen == screenType.applet) {

			content = this.getContentPane();
			content.setPreferredSize(new Dimension(800, 600));

		}

		JavaJumper.panel = new GameManager(this, this.keyboard);
		JavaJumper.panel.start();
		content.add(JavaJumper.panel);
		if (screen == screenType.window) {
			window.setIconImages(ArtsLoader.icons);
			window.addKeyListener(keyboard);
			window.pack();
			window.setLocationRelativeTo(null);
			window.setVisible(true);
			this.setFocusable(true);
			this.requestFocus();

		} else if (screen == screenType.applet) {
			this.addKeyListener(keyboard);
			this.setVisible(true);
			this.setFocusable(true);
			this.requestFocus();
		}
	}
}
