package de.champany.javajumper;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import de.champany.lib.SpriteMap;
import de.champany.lib.CTools;

public class ArtsLoader {
	public static SpriteMap player;
	public static SpriteMap player2;
	public static BufferedImage background, loading;
	public static ArrayList<BufferedImage> icons = new ArrayList<BufferedImage>();
	public static ArrayList<BufferedImage> levels = new ArrayList<BufferedImage>();
	public static SpriteMap block;
	
	public static final int levelID = 3;
	public static final int graphicsID = 1;

	public ArtsLoader() {
		this.loadImages();
	}

	public void loadImages() {
		String graphicsPath = "/img/game/graphics"+String.format("%03d", graphicsID)+"/";
		String levelPath = "/img/game/level"+String.format("%03d", levelID)+"/";		
		ArtsLoader.background = CTools.loadImage(graphicsPath+"background.jpg");
		ArtsLoader.block = SpriteMap.loadAndSplitIntoSquaresbyAmountY(
				graphicsPath+"blocks.png", 16);
		ArtsLoader.player = SpriteMap.splitIntoSquaresByAmountY(
				CTools.loadImage(graphicsPath+"player.png"), 4);
		//ArtsLoader.player2 = SpriteMap.splitBySize(CTools.loadImage("/img/game/player.png"), 16, 24);
		this.loadIcons();
		this.loadLevels(levelPath);
		ArtsLoader.loading = CTools.loadImage("/img/gui/loading.png");
	}
	
	public void loadIcons() {
		icons.add(CTools.loadImage("/img/gui/icons/16.png"));
		icons.add(CTools.loadImage("/img/gui/icons/32.png"));
		icons.add(CTools.loadImage("/img/gui/icons/64.png"));
		icons.add(CTools.loadImage("/img/gui/icons/128.png"));
	}
	
	public void loadLevels(String levelPath) {
		int n=0;

		while(true) {
			BufferedImage img = null;
			try {
				img = ImageIO.read(SpriteMap.class.getResourceAsStream(
						levelPath+String.format("%03d", n)+".png"));
				levels.add(img);
			} catch (Exception ex) {
				return;
			}
			n++;
		}
	}
}
