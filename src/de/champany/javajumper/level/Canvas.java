package de.champany.javajumper.level;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.player.Player;
import de.champany.javajumper.level.entitystoragesystem.CollisionAble;
import de.champany.javajumper.screen.ScreenSize;
import de.champany.lib.CMath;

public class Canvas implements CollisionAble {
	
	public Point2D center;
	public Entity trackedObject;
	public Rectangle2D canvas;
	
	public Graphics2D g;
	
	public Level level;
	
	public double scaleFactor = 1.0;
	public final static double[] availableScaleLevels = {0.25,0.5,1,2,3,4,6,8,10,15,20,50,100};
	public int scaleLevel = 0;
	public boolean autoScaleLevel = true;
		
	public Canvas(Level level, float scaleFactor, Point2D center, Dimension2D outputSize) {
		this.level = level;
		this.center = center;
		this.scaleFactor = scaleFactor;
	}
	
	public void trackObject(Player e) {
		this.trackedObject = e;
	}
	
	public void decreaseScaleLevel() {
		scaleLevel--;
		if(scaleLevel<0) {
			autoScaleLevel=true;
			scaleLevel=0;
		}
	}
	
	public void increaseScaleLevel() {
		scaleLevel++;
		if(autoScaleLevel) {
			autoScaleLevel=false;
			scaleLevel=0;
		}
		if(scaleLevel>=availableScaleLevels.length) {
			scaleLevel=availableScaleLevels.length-1;
		}
	}
	
	public void calculateScaleFactor() {
		if(autoScaleLevel) {
		/* Uses width or height for calculating scale
		 * this.scaleFactor=Math.floor(Math.max(16,Math.max(
						this.level.gameManager.screensize.getHeight()/
						this.level.size.getHeight(),
						this.level.gameManager.screensize.getWidth()/
						this.level.size.getWidth())));*/
		this.scaleFactor=Math.floor(Math.max(this.level.blockPixelHeight,
			this.level.gameManager.screensize.getHeight()/
			this.level.size.getHeight()));	
		} else {
			this.scaleFactor=this.level.blockPixelHeight*availableScaleLevels[scaleLevel];
		}
	}
	
	public void calculateCanvas(ScreenSize screenSize) {
		double canvasX,canvasY,canvasWidth,canvasHeight;
		this.calculateScaleFactor();
		canvasWidth=(screenSize.getWidth()/this.scaleFactor);
		canvasHeight=(screenSize.getHeight()/this.scaleFactor);

		this.center = this.trackedObject.getCenter();

		if(canvasWidth >= this.level.size.getWidth()) {
			canvasX=this.level.size.getCenterX()-(canvasWidth/2);
		} else {
			canvasX=Math.max(Math.min(this.center.getX()-(canvasWidth/2),this.level.size.getMaxX()-canvasWidth),this.level.size.getMinX());
		}
		
		if(canvasHeight >= this.level.size.getHeight()) {
			canvasY=this.level.size.getCenterY()-(canvasHeight/2);
		} else {
			canvasY=Math.max(Math.min(this.center.getY()-(canvasHeight/2),this.level.size.getMaxY()-canvasHeight),this.level.size.getMinY());
		}		
		
		this.canvas = new Rectangle2D.Double(
				canvasX,
				canvasY,
				canvasWidth,
				canvasHeight);
	}
	
	public void collidedBy(Entity entity) {
		entity.paint(this);
	}
	
	public void paint(BufferedImage sprite,double x, double y, double width, double height) {
		g.drawImage(sprite,
				(int)((x-this.canvas.getX())*this.scaleFactor),
				(int)(Math.floor((y-this.canvas.getY())*this.scaleFactor)),
				(int)(width*this.scaleFactor),
				(int)(height*this.scaleFactor),
				null);
	}
	
	public void paintLong(BufferedImage sprite,double x, double y, double width, double height) {
		int split=(int)Math.floor((sprite.getWidth()/2)-1);
		double splitPart = (double)split/(double)sprite.getWidth();
		int splitPointX[] = new int[4];
		splitPointX[0]=(int)((x-this.canvas.getX())*this.scaleFactor);
		splitPointX[1]=(int)((x-this.canvas.getX()+splitPart)*this.scaleFactor);
		splitPointX[2]=(int)(int)((x+width-this.canvas.getX()-splitPart)*this.scaleFactor);
		splitPointX[3]=(int)(int)((x+width-this.canvas.getX())*this.scaleFactor);
		int pointY= (int)(Math.floor((y-this.canvas.getY())*this.scaleFactor));
		int heightY=(int)Math.floor(height*this.scaleFactor);
		g.drawImage(sprite.getSubimage(0,0,split,sprite.getHeight()),
				splitPointX[0],
				pointY,
				splitPointX[1]-splitPointX[0],
				heightY,
				null);
		/*g.setPaint(new TexturePaint(sprite.getSubimage(split,0,sprite.getWidth()-2*split,sprite.getHeight()),
				new Rectangle(splitPointX[1],
						pointY,
						(int)(this.scaleFactor*(1-2*splitPart)),
						heightY-1)));
		//TODO This workaround (-1) 
		g.fillRect(splitPointX[1],
				pointY,
				splitPointX[2]-splitPointX[1],
				heightY);*/
		g.drawImage(sprite.getSubimage(split,0,1,sprite.getHeight()), splitPointX[1], pointY, splitPointX[2]-splitPointX[1], heightY, null);
		g.drawImage(sprite.getSubimage(sprite.getWidth()-split,0,split,sprite.getHeight()),
				splitPointX[2],
				pointY,
				splitPointX[3]-splitPointX[2],
				heightY,
				null);		
	}
	
	public BufferedImage render(ScreenSize screenSize) {
		this.calculateCanvas(screenSize);
		BufferedImage rendered = new BufferedImage(CMath.min(1,(int)(this.canvas.getWidth() * this.scaleFactor)), CMath.min(1,(int)(this.canvas.getHeight() * this.scaleFactor)),
				BufferedImage.TYPE_INT_ARGB);
		g = (Graphics2D) rendered.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		this.paintTiledImage(g, level.background, (int)(this.canvas.getX()*scaleFactor*(-0.5)+this.level.gameManager.tick*(0.2)),(int) (this.canvas.getY()*scaleFactor*0.1), 0, 0, (int)(this.canvas.getWidth()*this.scaleFactor), (int)(this.canvas.getHeight()*this.scaleFactor), level.background.getWidth(), level.background.getHeight());
		this.level.entities.checkCollision(this.canvas, this, this);
		return rendered;
	}
	
	public void paintTiledImage(Graphics2D g, BufferedImage img, int refX, int refY, int canvasX, int canvasY, int canvasWidth, int canvasHeight, int imgWidth, int imgHeight) {
		
		int startX = refX+(int)(CMath.cCeil((canvasX-refX)/imgWidth)-1)*imgWidth;
		//System.out.println(((canvasX-refX)/imgWidth));
		int startY = refY+(int)(CMath.cCeil((canvasY-refY)/imgHeight)-1)*imgHeight;
		
		int timesX = CMath.cCeil((canvasX+canvasWidth-startX)/imgWidth);
		int timesY = CMath.cCeil((canvasY+canvasHeight-startY)/imgHeight);
		
		//System.out.printf("%d|%d %dx%d%n",startX,startY,timesX,timesY);
		g.clip(new Rectangle(canvasX, canvasY, canvasWidth,canvasHeight));
		for(int x=0;x<=timesX;x++) {
			for(int y=0;y<=timesY;y++) {
				g.drawImage(img,startX+x*imgWidth,startY+y*imgHeight,imgWidth, imgHeight, null);
			}
		}
		g.setClip(null);
	}
}