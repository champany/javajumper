package de.champany.javajumper.level.entity;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import de.champany.javajumper.level.Canvas;
import de.champany.javajumper.level.Level;
import de.champany.lib.CollisionBox;
import de.champany.lib.SpriteBox;

public abstract class Entity {
	public Point2D.Double point;
	public SpriteBox sprite;
	public CollisionBox collision;
	public boolean physics, visible, collisionAble;

	public Level level;

	public Entity(BufferedImage sprite, double x, double y, double paddingUp,
			double paddingRight, double paddingDown, double paddingLeft,
			double spritePaddingUp, double spritePaddingRight,
			double spritePaddingDown, double spritePaddingLeft) {
		this.point = new Point2D.Double(x, y);
		this.collision = new CollisionBox(this.point, paddingUp, paddingRight,
				paddingDown, paddingLeft);
		this.sprite = new SpriteBox(sprite, this.point, spritePaddingUp,
				spritePaddingRight, spritePaddingDown, spritePaddingLeft);
	}
	
	public void remove() {
		this.collisionAble = false;
		this.physics = false;
		this.visible = false;
	}

	public Entity(BufferedImage sprite, double x, double y, double paddingUp,
			double paddingRight, double paddingDown, double paddingLeft) {
		this(sprite, x, y, paddingUp, paddingRight, paddingDown, paddingLeft,
				paddingUp, paddingRight, paddingDown, paddingLeft);
	}

	public Entity(double x, double y, double paddingUp, double paddingRight,
			double paddingDown, double paddingLeft) {
		this(null, x, y, paddingUp, paddingRight, paddingDown, paddingLeft,
				paddingUp, paddingRight, paddingDown, paddingLeft);
	}
	
	public void setSprite(BufferedImage spriteImg) {
		this.sprite.setImage(spriteImg);
	}
	
	public Point2D getCenter() {
		return new Point2D.Double(this.collision.getCenterX(),this.collision.getCenterY());
	}

	public void paint(Canvas canvas) {	
		if ((this.sprite != null) && (this.visible == true)) {
			canvas.paint(this.sprite.getImage(),
					this.point.x-sprite.padding[SpriteBox.sLEFT],
					this.point.y-sprite.padding[SpriteBox.sUP],
					this.sprite.getWidth(),
					this.sprite.getHeight());
		}

	}

	public void setLevel(Level level) {
		this.level = level;
	}
	
	public abstract void interact(Entity entity, int direction);
	
	public abstract void update();
	
	public abstract void doTick();
}