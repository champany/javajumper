package de.champany.javajumper.level.entity.block;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.player.Player;

public class AimBlock extends Block {
	public boolean activated = false;
	
	public AimBlock(double x, double y) {
		super(ArtsLoader.block.getSprite(55), x, y, 0, 1, 1, 0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
	}

	public void interact(Entity entity, int direction) {
		if(entity instanceof Player) {
			
			if(!activated) this.level.finish();
			activated = true;
		}
	}
}
