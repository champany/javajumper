package de.champany.javajumper.level.entity.block;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.Mob;
import de.champany.javajumper.level.entity.mob.player.Player;
import de.champany.javajumper.level.entitystoragesystem.TickAble;
public class CrackedBlock extends Block implements TickAble {
	
	public int LIFETIME;
	public int time;
	public boolean isTouched, isTouchedHard;
	
	public CrackedBlock (double x, double y, int liveTicks) {
		super(null, x, y, 0, 1, 1, 0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
		this.sprite.image = ArtsLoader.block.getSprite(16);
		this.LIFETIME = liveTicks;
		this.time = LIFETIME;
	}

	public void interact(Entity entity, int direction) {
		//TODO CHECK, whether Player above JumpPad
		if(entity instanceof Mob) {
			this.isTouched = true;
			if(entity instanceof Player && ((Player)entity).isInteracting()) {
				this.isTouchedHard = true;
			}
		}
	}
	
	@Override
	public void doTick() {
		if(isTouched){
			time-=isTouchedHard?2:1;
		}
		isTouched = false;
		isTouchedHard = false;
		
		if (this.time >= this.LIFETIME * 0.8) {
			this.setSprite(ArtsLoader.block.getSprite(20));			
		} else if (this.time >= this.LIFETIME * 0.6) {
			this.setSprite(ArtsLoader.block.getSprite(21));			
		} else if (this.time >= this.LIFETIME * 0.4) {
			this.setSprite(ArtsLoader.block.getSprite(22));			
		} else if (this.time >= this.LIFETIME * 0.2) {
			this.setSprite(ArtsLoader.block.getSprite(23));			
		} else if (this.time >= 0) {
			this.setSprite(ArtsLoader.block.getSprite(24));			
		} else {
			this.remove();
		}
	}
}
