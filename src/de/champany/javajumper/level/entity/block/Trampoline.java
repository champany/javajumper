package de.champany.javajumper.level.entity.block;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.Canvas;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.player.Player;
import de.champany.javajumper.level.entitystoragesystem.TickAble;
import de.champany.lib.SpriteBox;

public class Trampoline extends Block implements TickAble, LongBlock {
	
	public int lastJump = 0;
	public int animationLength = 10;
	public SpriteBox initialSprite;

	
	public Trampoline(double x, double y) {
		super(null, x, y, 0, 1, 1, 0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
		this.sprite.image = ArtsLoader.block.getSprite(16);
		this.initialSprite=this.sprite;
	}

	public void interact(Entity entity, int direction) {
		//TODO CHECK, whether Player above JumpPad
		if(entity instanceof Player) {
			if(direction == Player.DOWN) {
				if(((Player)entity).jumpTrampoline(1.2d)) {
					this.lastJump = animationLength;
				} else {
					this.lastJump = -animationLength;
				}
			}
		}
	}
	
	public void increaseWidth() {
		this.collision.extendRight(1);
		this.sprite.extendRight(1);
	}

	public void paint(Canvas canvas) {
		if ((this.sprite != null) && (this.visible == true)) {
			if(this.sprite.getWidth()==1)
			canvas.paint(this.sprite.getImage(),
					this.point.x-sprite.padding[SpriteBox.sLEFT],
					this.point.y-sprite.padding[SpriteBox.sUP],
					this.sprite.getWidth(),
					this.sprite.getHeight());
			else
				canvas.paintLong(this.sprite.getImage(),
						this.point.x-sprite.padding[SpriteBox.sLEFT],
						this.point.y-sprite.padding[SpriteBox.sUP],
						this.sprite.getWidth(),
						this.sprite.getHeight());

				
		}

	}
	
	@Override
	public void doTick() {
		if(lastJump != 0) {
			lastJump += (lastJump > 0) ? -1 : 1;
			if(lastJump >  (animationLength/2)) {
				this.setSprite(ArtsLoader.block.getSprite(18));
			} else if(lastJump > 0) {
				this.setSprite(ArtsLoader.block.getSprite(17));
			} else if(lastJump == 0){
				this.setSprite(ArtsLoader.block.getSprite(16));
			} else if(lastJump < 0) {
				this.setSprite(ArtsLoader.block.getSprite(19));
			}
		}
	}
}