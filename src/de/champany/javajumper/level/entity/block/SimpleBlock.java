package de.champany.javajumper.level.entity.block;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.Canvas;
import de.champany.javajumper.level.entity.Entity;
import de.champany.lib.SpriteBox;

public class SimpleBlock extends Entity implements LongBlock {
	public static final int YELLOW = 0, ORANGE = 1, RED = 2, ROSE = 3,
			PINK = 4, PURPLE = 5, BLUE = 6, LIGHT_BLUE = 7, CYAN = 8,
			GREEN = 9, DARK_GREEN = 10, BROWN = 11, BLACK = 12, GREY = 13,
			LIGHT_GREY = 14, WHITE = 15;
	public int subID;
	public SpriteBox initialSprite;

	public SimpleBlock(int subID, double x, double y) {
		super(null, x, y, 0, 1, 1, 0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
		this.initialSprite=this.sprite;
		if ((0 <= subID) && (subID <= 15)) {
			this.subID = subID;
		} else {
			this.subID = 0;
		}
		this.sprite.image = ArtsLoader.block.getSprite(subID);
	}

	public void increaseWidth() {
		this.collision.extendRight(1);
		this.sprite.extendRight(1);
	}
	
	@Override
	public void doTick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void interact(Entity entity, int direction) {

	}
	
	public void paint(Canvas canvas) {
		if ((this.sprite != null) && (this.visible == true)) {
			if(this.sprite.getWidth()==1)
			canvas.paint(this.sprite.getImage(),
					this.point.x-sprite.padding[SpriteBox.sLEFT],
					this.point.y-sprite.padding[SpriteBox.sUP],
					this.sprite.getWidth(),
					this.sprite.getHeight());
			else
				canvas.paintLong(this.sprite.getImage(),
						this.point.x-sprite.padding[SpriteBox.sLEFT],
						this.point.y-sprite.padding[SpriteBox.sUP],
						this.sprite.getWidth(),
						this.sprite.getHeight());

				
		}

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
}