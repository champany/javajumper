package de.champany.javajumper.level.entity.block.lever;

import java.util.Vector;

import de.champany.javajumper.level.entity.Entity;

public class LeverSystem {
	public static final int RED = 0, GREEN = 1, BLUE= 2, YELLOW = 3;
	public int colorID = 0;
	public boolean isActive = false;
	public Vector<Entity> entities = new Vector<Entity>();
	
	public LeverSystem(int colorID) {
		this.colorID = colorID;
	}
	
	public void addEntity(Entity entity2add) {
		if (entity2add != null) {
			this.entities.add(entity2add);
		}
	}
	
	public void update() {
		for (Entity entity : this.entities) {
			entity.update();
		}

	}
	
	public void switchState() {
		this.isActive = !this.isActive;
		this.update();
	}
}
