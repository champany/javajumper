package de.champany.javajumper.level.entity.block.lever;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.Canvas;
import de.champany.javajumper.level.entity.block.Block;
import de.champany.javajumper.level.entity.block.LongBlock;
import de.champany.javajumper.level.entitystoragesystem.TickAble;
import de.champany.lib.SpriteBox;

public class SwitchableBlock extends Block implements TickAble, LongBlock {

	private LeverSystem leversystem;
	boolean inverted;
	public SpriteBox initialSprite;

	
	public SwitchableBlock(double x, double y, LeverSystem leversystem, boolean inverted) {
		super(null, x, y, 0,1,1,0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
		this.leversystem = leversystem;
		this.inverted = inverted;
		this.leversystem.addEntity(this);
		this.initialSprite=this.sprite;

		this.update();
	}
	
	public void paint(Canvas canvas) {
		if ((this.sprite != null) && (this.visible == true)) {
			if(this.sprite.getWidth()==1)
			canvas.paint(this.sprite.getImage(),
					this.point.x-sprite.padding[SpriteBox.sLEFT],
					this.point.y-sprite.padding[SpriteBox.sUP],
					this.sprite.getWidth(),
					this.sprite.getHeight());
			else
				canvas.paintLong(this.sprite.getImage(),
						this.point.x-sprite.padding[SpriteBox.sLEFT],
						this.point.y-sprite.padding[SpriteBox.sUP],
						this.sprite.getWidth(),
						this.sprite.getHeight());

				
		}

	}
	
	public void increaseWidth() {
		this.collision.extendRight(1);
		this.sprite.extendRight(1);
	}

	public void update() {
		this.setSprite(ArtsLoader.block.getSprite(47+this.leversystem.colorID+((leversystem.isActive^this.inverted)?0:4)));
		this.collisionAble = (leversystem.isActive^this.inverted);
	}
	
}
