package de.champany.javajumper.level.entity.block.lever;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.block.Block;
import de.champany.javajumper.level.entity.mob.player.Player;
import de.champany.javajumper.level.entitystoragesystem.TickAble;

public class LeverBlock extends Block implements TickAble {

	private int cooldown;
	private LeverSystem leversystem;
	
	public LeverBlock(double x, double y, LeverSystem leversystem) {
		super(null, x, y, 0,1,1,0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
		this.leversystem = leversystem;
		this.leversystem.addEntity(this);
		this.update();
	}
	
	public void update() {
		this.setSprite(ArtsLoader.block.getSprite(39+this.leversystem.colorID+(leversystem.isActive?0:4)));
	}
	
	public void doTick() {
		if(cooldown != 0) {
			cooldown--;
			if(cooldown < 0) cooldown = 0;
		}
	}

	public void interact(Entity entity, int direction) {
		if(entity instanceof Player) {
			if(((Player) entity).isInteracting() && (this.cooldown == 0)) {
				cooldown = 60;
				leversystem.switchState();
			}
		}
	}

}
