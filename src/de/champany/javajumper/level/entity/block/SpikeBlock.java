package de.champany.javajumper.level.entity.block;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.player.Player;

public class SpikeBlock extends Block {
	
	public SpikeBlock(double x, double y) {
		super(null, x, y, 0, 1, 1, 0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
		this.sprite.image = ArtsLoader.block.getSprite(57);
	}
	
	public void interact(Entity entity, int direction) {
		if(entity instanceof Player) {
			((Player)entity).die();
		}
	}
}
