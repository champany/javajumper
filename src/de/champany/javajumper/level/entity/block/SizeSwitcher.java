package de.champany.javajumper.level.entity.block;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.player.Player;
import de.champany.javajumper.level.entitystoragesystem.TickAble;

public class SizeSwitcher extends Block implements TickAble {

	public int cooldown = 0;
	
	public SizeSwitcher(double x, double y) {
		super(ArtsLoader.block.getSprite(37), x, y, 0, 1, 1, 0);
		this.physics = false;
		this.visible = true;
		this.collisionAble = true;
	}
	
	@Override
	public void doTick() {
		this.setSprite(ArtsLoader.block.getSprite(level.player.isSmall?38:37));
		if(cooldown != 0) {
			cooldown--;
			if(cooldown < 0) cooldown = 0;
		}
	}

	public void interact(Entity entity, int direction) {
		if(entity instanceof Player) {
			if(((Player) entity).isInteracting() && (this.cooldown == 0) && (direction == Player.DOWN)) {
				//TODO FIX isPressed
				cooldown = 60;
				((Player) entity).changeSize();
			}
		}
	}
}
