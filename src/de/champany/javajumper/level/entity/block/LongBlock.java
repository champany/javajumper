package de.champany.javajumper.level.entity.block;

public interface LongBlock {
	public abstract void increaseWidth();
}
