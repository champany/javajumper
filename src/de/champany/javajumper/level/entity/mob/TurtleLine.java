package de.champany.javajumper.level.entity.mob;

import de.champany.javajumper.ArtsLoader;

public class TurtleLine extends Turtle {
	
	public TurtleLine(double x, double y, double speed, int startWith) {
		super(x, y, speed, startWith);
		this.direction = startWith;
		this.update();
	}
	
	public void update() {
		if(this.speedX==0 && this.speedY==0) {
			this.swapDirection();
 		}
				
		if(direction == UP) {
			speedX = 0;
			speedY = -speed;
			this.setSprite(ArtsLoader.block.getSprite(36));			
		} else if(direction == RIGHT) {
			speedX = speed;
			speedY = 0;
			this.setSprite(ArtsLoader.block.getSprite(33));			
		} else if(direction == LEFT) {
			speedX = -speed;
			speedY = 0;
			this.setSprite(ArtsLoader.block.getSprite(35));			
		} else if(direction == DOWN) {
			speedX = 0;
			speedY = speed;
			this.setSprite(ArtsLoader.block.getSprite(34));			
		}
	}

	@Override
	protected void swap() {
		this.swapDirection();
	}
}
