package de.champany.javajumper.level.entity.mob;

import java.awt.image.BufferedImage;

import de.champany.javajumper.level.Level;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entitystoragesystem.TickAble;
import de.champany.lib.CMath;
import de.champany.lib.SpriteBox;

public abstract class MovableEntity extends Entity implements TickAble {

	public double speedX, speedY; // in Blocks per tick
	public static final int RIGHT = 0, DOWN = 1, LEFT = 2, UP = 3;
	public boolean pushAble;

	public MovableEntity (BufferedImage sprite, double x, double y,
			double paddingUp, double paddingRight, double paddingDown,
			double paddingLeft, double spritePaddingUp,
			double spritePaddingRight, double spritePaddingDown,
			double spritePaddingLeft) {
		super(sprite, x, y, paddingUp, paddingRight, paddingDown, paddingLeft);
		this.physics = true;
		this.pushAble = false;
		this.visible = true;
		this.collisionAble = true;
		this.sprite = new SpriteBox(sprite, this.point, spritePaddingUp,
				spritePaddingRight, spritePaddingDown, spritePaddingLeft);
	}

	public MovableEntity(BufferedImage sprite, double x, double y,
			double paddingUp, double paddingRight, double paddingDown,
			double paddingLeft) {
		this(sprite, x, y, paddingUp, paddingRight, paddingDown, paddingLeft,
				paddingUp, paddingRight, paddingDown, paddingLeft);
	}

	public MovableEntity(double x, double y, double paddingUp,
			double paddingRight, double paddingDown, double paddingLeft) {
		this(null, x, y, paddingUp, paddingRight, paddingDown, paddingLeft,
				paddingUp, paddingRight, paddingDown, paddingLeft);
	}
	
	public boolean push(int direction, double distance) {
		if(pushAble) {
			//TODO: Test if works without it
			distance += Level.accuracy;
			
			double distanceY = 0, distanceX = 0;
			switch(direction) {
			case MovableEntity.UP: distanceY=-distance; break;
			case MovableEntity.DOWN: distanceY=distance; break;
			case MovableEntity.LEFT:  distanceX=-distance; break;
			case MovableEntity.RIGHT:  distanceX=distance;  break;
			default: return false;
			}
			
			collision.doTmpMovement(distanceX, distanceY);
			this.inBlock(true, direction);
			if (this.inBlock()) {
				collision.discardTmpMovement();
				return false;
			} else {
				collision.applyTmpMovement();
				return true;
			}
		} else {
			return false;
		}
	}

	@Override
	public void doTick() {
		this.collision.discardTmpMovement();
		if (speedX != 0 || speedY != 0) {
			doMovementTick();
		}
		if (physics) {
			doPhysicsTick();
		}
	}

	public void doPhysicsTick() {
		if (!onBlock(true)) {
			this.speedY += Level.gravitation;
		} else {
			// this.speedY = 0;
		}
		this.fixSpeed();
	}
	
	public void fixSpeed() {
		speedX = CMath.getInRangeByCutting(speedX,Level.maxSpeed);
		speedY = CMath.getInRangeByCutting(speedY,Level.maxSpeed);
	}
	
	public void doMovementTick() {
		if (!this.inBlock()) {
			int steps = Math.max(
					1,
					(int) Math.floor(Math.sqrt(speedX * speedX + speedY
							* speedY)
							/ Level.accuracy));
			for (int i = 0; i <= steps; i++) {
				this.move(speedX / steps, speedY / steps);
			}
		} else {
			// TODO WTF did I do here?
			this.level.playerDied();
		}
	}
	
	public void collidedBy(Entity e) {
		
	}

	public void move(double x, double y) {
		this.fixSpeed();
		if(x != 0) {
			collision.doTmpMovement(x, 0);
			this.inBlock(true, (y > 0) ? MovableEntity.RIGHT : MovableEntity.LEFT);
			if (this.inBlock()) {
				speedX = 0;
				collision.discardTmpMovement();
			} else {
				collision.applyTmpMovement();
			}
		}
		if(y != 0) {
			collision.doTmpMovement(0, y);
			//TODO Better solution
			this.inBlock(true, (y > 0) ? MovableEntity.DOWN : MovableEntity.UP);
			if (this.inBlock()) {
				speedY = 0;
				collision.discardTmpMovement();
			} else {
				collision.applyTmpMovement();
			}
		}
		this.level.entities.register(this);
	}

	public boolean onBlock(boolean interact) {
		this.collision.doTmpMovement(0, Level.accuracy);
		boolean returning = this.inBlock(interact, MovableEntity.DOWN);
		this.collision.discardTmpMovement();
		return returning;
	}
	
	public boolean inBlock(boolean doInteract, int direction) {
		boolean returning = false;
		if (collisionAble == false)
			return false;
		for (Entity entity : level.entities.entities) {
			if (entity.collisionAble && entity != this) {
				if (this.collision.intersects(entity.collision)) {
					returning |= true;
					if (doInteract) {
						entity.interact(this, direction);
					}
				}
			}
		}
		return returning;
	}

	public boolean inBlock() {
		return this.inBlock(false, 0);
	}
	
	public boolean isMoving() {
		return (this.isMovingY()) || (this.isMovingX());
	}
	
	public boolean isMovingY() {
		return (Math.abs(this.speedY) > Level.accuracy);
	}
	
	public boolean isMovingX() {
		return (Math.abs(this.speedX) > Level.accuracy);
	}
}