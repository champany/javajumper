package de.champany.javajumper.level.entity.mob;

import de.champany.javajumper.ArtsLoader;

public class TurtleQuad extends Turtle {

	protected boolean clockwise;

	public TurtleQuad(double x, double y, double speed, int startWith) {
		super(x, y, speed, startWith);
		if(speed < 0) {
			this.clockwise = false;
		} else if(speed > 0) {
			this.clockwise = true;
		} else if(speed == 0) {
			this.clockwise = true;
			System.err.println("[Slider] init /wo speed");
		}
		
		this.direction = startWith;
		this.update();
	}
	
	public void update() {
		if(this.speedX==0 && this.speedY==0) {
			direction += clockwise ? 1 : -1;
 		}
		
		if(swap) {
			this.swap = false;
		}

		if(direction > 3) {
			direction = 0;
		} else if(direction < 0) {
			direction = 3;
		}
		
		if(direction == UP) {
			speedX = 0;
			speedY = -speed;
			this.setSprite(ArtsLoader.block.getSprite(this.clockwise?28:32));			
		} else if(direction == RIGHT) {
			speedX = speed;
			speedY = 0;
			this.setSprite(ArtsLoader.block.getSprite(this.clockwise?25:29));			
		} else if(direction == LEFT) {
			speedX = -speed;
			speedY = 0;
			this.setSprite(ArtsLoader.block.getSprite(this.clockwise?27:31));			
		} else if(direction == DOWN) {
			speedX = 0;
			speedY = speed;
			this.setSprite(ArtsLoader.block.getSprite(this.clockwise?26:30));			
		}
	}

	@Override
	protected void swap() {
		this.clockwise = !this.clockwise;
		this.swapDirection();
	}
}
