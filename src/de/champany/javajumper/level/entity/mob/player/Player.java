package de.champany.javajumper.level.entity.mob.player;

import de.champany.javajumper.Logger;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.Mob;
import de.champany.lib.Keyboard;
import de.champany.lib.SpriteMap;

public class Player extends Mob {
	public Keyboard keyboard;
	public int jumpKey, rightKey, leftKey, useKey;
	public boolean interact = false;
	public int animationTick, subAnimationTick;
	public boolean isSmall = false;
	public boolean onBlock = true;
	public boolean runOnlyRight = true;
	public boolean pushAble=true;
	
	public static final double accelerationXBig = 0.07d,
			stopAccelerationFactorBig = 0.35d, maxSpeedXBig = 0.12d,
			maxJumpSpeedBig = 0.21d, minJumpSpeedBig = 0.18d;
	public static final double accelerationXSmall = 0.06d,
			stopAccelerationFactorSmall = 0.7d, maxSpeedXSmall = 0.14d,
			maxJumpSpeedSmall = 0.14d, minJumpSpeedSmall = 0.12d;

	public double accelerationX = 0.06d,
			stopAccelerationFactor = 0.7d, maxSpeedX = 0.12d,
			maxJumpSpeed = 0.21d, minJumpSpeed = 0.18d;

	
	public int animationFrameLength = 20, animationFrameAmount;
	public SpriteMap spriteMap;
	
	
	public Player(double x, double y, Keyboard keyboard, int jumpKey,
			int leftKey, int rightKey, int useKey, SpriteMap spriteMap, boolean runOnlyRight) {
		super(null, x+1, y-0.2, 0,-0.2,  1.2, .8, 
				0.8, 1, 2.2, 2);
		this.spriteMap = spriteMap;
		this.animationFrameAmount = this.spriteMap.length;
		this.keyboard = keyboard;
		this.jumpKey = jumpKey;
		this.rightKey = rightKey;
		this.leftKey = leftKey;
		this.useKey = useKey;
		this.runOnlyRight = runOnlyRight;
		if(this.runOnlyRight) this.speedX = maxSpeedX;
		update();
	}

	public void update() {
		if(this.isMoving()) {
			if(this.subAnimationTick >= this.animationFrameLength) {
				subAnimationTick = 0;
				animationTick++;
			}
			
			if(this.animationTick >= (this.animationFrameAmount/4)) {
				animationTick = 0;
			}
		}
				
		this.sprite.setImage(this.spriteMap.getSprite(animationTick + (!this.onBlock?4:0)+((this.isSmall)?8:0)));
		
		Logger.getInstance().setHighscore(this.point.getX());
		
		this.subAnimationTick++;
		
		if(isSmall) {
			this.accelerationX = Player.accelerationXSmall;
			this.stopAccelerationFactor = Player.stopAccelerationFactorSmall;
			this.maxSpeedX = Player.maxSpeedXSmall;
			this.maxJumpSpeed = Player.maxJumpSpeedSmall;
			this.minJumpSpeed = Player.minJumpSpeedSmall;
		} else {
			this.accelerationX = Player.accelerationXBig;
			this.stopAccelerationFactor = Player.stopAccelerationFactorBig;
			this.maxSpeedX = Player.maxSpeedXBig;
			this.maxJumpSpeed = Player.maxJumpSpeedBig;
			this.minJumpSpeed = Player.minJumpSpeedBig;
		}
	}
	
	
	public void doTick() {
		if(this.point.getY() >= (this.level.size.getHeight()+10)) this.die();
		super.doTick();
		if (this.runOnlyRight || keyboard.isKeyPressed(rightKey)) {
			this.speedX = Math.min(this.speedX + this.accelerationX,
					maxSpeedX);
			this.sprite.flipVertical = false;
		} else if (keyboard.isKeyPressed(leftKey)) {
			this.speedX = Math.max(this.speedX - this.accelerationX,
					-maxSpeedX);
			this.sprite.flipVertical = true;
		} else {
			if (speedX != 0) {
				if (speedX > 0) {
					speedX -= stopAccelerationFactor * this.accelerationX;
					if (speedX < 0) {
						speedX = 0;
					}
				} else if (speedX < 0) {
					speedX += stopAccelerationFactor * this.accelerationX;
					if (speedX > 0) {
						speedX = 0;
					}
				}
			}
		}

		if (keyboard.isKeyPressed(jumpKey))
			this.jump();
		
		if(this.runOnlyRight && this.speedX<this.maxSpeedX) this.die();

		this.update();
	}
	
	public boolean isInteracting() {
		return keyboard.isKeyPressed(useKey);
	}
	
	public boolean hasInteracted() {
		return keyboard.isKeyTyped(useKey);
	}

	public void jump() {
		jump(-this.minJumpSpeed + (-this.maxJumpSpeed + this.minJumpSpeed)* (Math.abs(this.speedX) / this.maxSpeedX));
	}
	
	public void die() {
		this.level.playerDied();
	}
	
	public void jump(double height) {
		if (onBlock(false)) {
			this.speedY = height;
		}
	}
	
	public boolean jumpTrampoline(double factor) {
		if(!isInteracting()) {
			jump(Math.min(-0.05d*factor,-Math.abs(speedY)*factor));
			return true;
		} else {
			return false;
		}
		
	}
	
	@Override
	public void interact(Entity entity, int direction) {
		// TODO Auto-generated method stub
		
	}
	
	public void changeSize() {
		if(this.isSmall) {
			this.changeSizeToBig();
		} else {
			this.changeSizeToSmall();
		}
	}
	
	public void changeSizeToBig() {
		this.collision.setPadding(0,-.2,  1.2, .8);
		this.isSmall = false;
	}
	
	public void changeSizeToSmall() {
		this.collision.setPadding(-.3,-.2,  1.2, .8);
		this.isSmall = true;
	}
	
	public boolean onBlock(boolean interact) {
		boolean onBlock = super.onBlock(interact);
		this.onBlock = onBlock;
		return onBlock;
		
	}
}
