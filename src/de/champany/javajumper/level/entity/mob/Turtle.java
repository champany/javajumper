package de.champany.javajumper.level.entity.mob;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.level.entity.Entity;
import de.champany.javajumper.level.entity.mob.player.Player;

public abstract class Turtle extends MovableEntity {
	protected double speed;
	protected int direction = 0;
	protected boolean swap;
	protected int swapCooldownTime = 0;
	protected int SWAPCOOLDOWNMAX = 30;
	
	public Turtle(double x, double y, double speed, int startWith) {
		super(ArtsLoader.block.getSprite(25), x, y,-0.08,0.92, 0.92, -0.08, 0, 1, 1, 0);
		this.physics = false;
		this.speed = Math.abs(speed);
	}

	protected void swapDirection() {
		switch(direction) {
		case Turtle.RIGHT: this.direction = Turtle.LEFT; break;
		case Turtle.DOWN: this.direction = Turtle.UP; break;
		case Turtle.LEFT: this.direction = Turtle.RIGHT; break;
		case Turtle.UP: this.direction = Turtle.DOWN; break;
		}
	}

	@Override
	public void interact(Entity entity, int direction) {
		if(entity instanceof Player) {
			if(((Player) entity).isInteracting() && this.swapCooldownTime <= 0) {
				this.swap = true;
				this.swapCooldownTime = this.SWAPCOOLDOWNMAX;
			}
		}
	}
	
	public abstract void update();
	
	public void doTick() {
		super.doTick();
		if(this.swapCooldownTime != 0) {
			this.swapCooldownTime--;
			if(swapCooldownTime < 0) swapCooldownTime = 0;
		}
		
		if(swap) {
			this.swap();
			this.update();
			swap = false;
		}
		
		if(this.speedX==0 && this.speedY==0) {
			this.update();
 		}

	}
	
	protected abstract void swap();
}
