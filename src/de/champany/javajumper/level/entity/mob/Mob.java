package de.champany.javajumper.level.entity.mob;

import java.awt.image.BufferedImage;

import de.champany.javajumper.level.entity.Entity;

public abstract class Mob extends MovableEntity {

	public Mob(BufferedImage sprite, double x, double y, double paddingUp,
			double paddingRight, double paddingDown, double paddingLeft,
			double spritePaddingUp, double spritePaddingRight,
			double spritePaddingDown, double spritePaddingLeft) {
		super(sprite, x, y, paddingUp, paddingRight, paddingDown, paddingLeft,
				spritePaddingUp, spritePaddingRight, spritePaddingDown,
				spritePaddingLeft);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void interact(Entity entity, int direction) {
		// TODO Auto-generated method stub
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void collidedBy(Entity e) {
		
		
	}


}
