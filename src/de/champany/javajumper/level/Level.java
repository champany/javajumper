package de.champany.javajumper.level;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import de.champany.javajumper.*;
import de.champany.javajumper.level.entity.*;
import de.champany.javajumper.level.entity.block.*;
import de.champany.javajumper.level.entity.block.lever.*;
import de.champany.javajumper.level.entity.mob.*;
import de.champany.javajumper.level.entity.mob.player.*;
import de.champany.javajumper.level.entitystoragesystem.LevelEntities;
import de.champany.javajumper.screen.*;
import de.champany.lib.Keyboard;

public class Level {
	public static Level loadFromImage(GameManager gm, GameScreen gs,
			BufferedImage level, int sizeFactor, BufferedImage background) {
		Level clevel = new Level(gm, gs, level.getWidth(),
				level.getHeight(), sizeFactor, background);
		for (int y = 0; y <= (level.getHeight() - 1); y++) {
			int lastColorHTML = -1;
			Entity lastBlock = null;
			for (int x = 0; x <= (level.getWidth() - 1); x++) {
				Color color = new Color(level.getRGB(x, y));
				int colorHTML = (color.getRed() * 256 * 256)
						+ (color.getGreen() * 256) + color.getBlue();
				if(lastColorHTML == colorHTML && (lastBlock instanceof LongBlock)) {
					((LongBlock)lastBlock).increaseWidth();
					continue;
				} else {
					lastBlock = null;
				}
				lastColorHTML = colorHTML;
				switch (colorHTML) {
				// Normale Blöcke
				case 0xFFFF00:
					lastBlock = new SimpleBlock(SimpleBlock.YELLOW, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xFF8800:
					lastBlock = new SimpleBlock(SimpleBlock.ORANGE, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xFF0000:
					lastBlock = new SimpleBlock(SimpleBlock.RED, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xFFAAAA:
					lastBlock = new SimpleBlock(SimpleBlock.ROSE, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xFF0066:
					lastBlock = new SimpleBlock(SimpleBlock.PINK, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xDD00DD:
					lastBlock = new SimpleBlock(SimpleBlock.PURPLE, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x0000FF:
					lastBlock = new SimpleBlock(SimpleBlock.BLUE, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x44ffff:
					lastBlock = new SimpleBlock(SimpleBlock.LIGHT_BLUE, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x88BBFF:
					lastBlock = new SimpleBlock(SimpleBlock.CYAN, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x00FF00:
					lastBlock = new SimpleBlock(SimpleBlock.GREEN, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x009900:
					lastBlock = new SimpleBlock(SimpleBlock.DARK_GREEN, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x995500:
					lastBlock = new SimpleBlock(SimpleBlock.BROWN, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x111111:
					lastBlock = new SimpleBlock(SimpleBlock.BLACK, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0x444444:
					lastBlock = new SimpleBlock(SimpleBlock.GREY, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xbbbbbb:
					lastBlock = new SimpleBlock(SimpleBlock.LIGHT_GREY, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xdddddd:
					lastBlock = new SimpleBlock(SimpleBlock.WHITE, x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xdddd00:
					lastBlock = new Trampoline(x, y);
					clevel.addEntity(lastBlock);
					break;
				case 0xaeaeae:
					clevel.addEntity(new CrackedBlock(x, y, 120));
					break;
				case 0x00aaff:
					clevel.addEntity(new TurtleQuad(x, y, 0.02d, Turtle.RIGHT));
					break;					
				case 0x449900:
					clevel.addEntity(new TurtleQuad(x, y, -0.02d, Turtle.RIGHT));
					break;	
				case 0xff9900:
					clevel.addEntity(new TurtleLine(x, y, -0.02d, Turtle.RIGHT));
					break;		
				case 0xffddcc:
					clevel.addEntity(new SizeSwitcher(x, y));
					break;
				case 0xbfbf00:
					clevel.addEntity(new LeverBlock(x, y, clevel.getLeverSystem(LeverSystem.YELLOW)));
					break;
				case 0xbf0000:
					clevel.addEntity(new LeverBlock(x, y, clevel.getLeverSystem(LeverSystem.RED)));
					break;	
				case 0x00bf00:
					clevel.addEntity(new LeverBlock(x, y, clevel.getLeverSystem(LeverSystem.BLUE)));
					break;
				case 0x0000bf:
					clevel.addEntity(new LeverBlock(x, y, clevel.getLeverSystem(LeverSystem.GREEN)));
					break;
				case 0xffff4d:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.YELLOW),true);
					clevel.addEntity(lastBlock);
					break;
				case 0xff4d4d:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.RED),true);
					clevel.addEntity(lastBlock);
					break;	
				case 0x4dff4d:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.BLUE),true);
					clevel.addEntity(lastBlock);
					break;
				case 0x4d4dff:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.GREEN),true);
					clevel.addEntity(lastBlock);
					break;					
				case 0xffff8d:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.YELLOW),false);
					clevel.addEntity(lastBlock);
					break;
				case 0xff8d8d:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.RED),false);
					clevel.addEntity(lastBlock);
					break;	
				case 0x8dff8d:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.GREEN),false);
					clevel.addEntity(lastBlock);
					break;
				case 0x8d8dff:
					lastBlock = new SwitchableBlock(x, y, clevel.getLeverSystem(LeverSystem.BLUE),false);
					clevel.addEntity(lastBlock);
					break;
				case 0x666666:
					clevel.addEntity(new AimBlock(x, y));
					break;
				case 0x88aa88:
					clevel.addEntity(new SpikeBlock(x, y));
					break;					
				case 0x000000:
					clevel.setPlayer(new Player(x, y, gm.keyboard,
							Keyboard.MOVE_JUMP, Keyboard.MOVE_LEFT,
							Keyboard.MOVE_RIGHT, Keyboard.MOVE_USE,ArtsLoader.player, false));
							break;
				case 0x020202:
					clevel.setPlayer(new Player(x, y, gm.keyboard,
							Keyboard.MOVE_JUMP, Keyboard.MOVE_LEFT,
							Keyboard.MOVE_RIGHT, Keyboard.MOVE_USE,ArtsLoader.player, true));
							break;
				/*case 0x010101:
					clevel.setPlayer(new Player(x, y, gm.keyboard,
							Keyboard.W, Keyboard.A,
							Keyboard.D, Keyboard.S,ArtsLoader.player2));
					break;*/
				}
			}
		}

		return clevel;
	}

	public Rectangle size;
	public LevelEntities entities;
	public BufferedImage background;
	public Player player;
	public LeverSystem[] leverSystems = new LeverSystem[4];
	public Canvas canvas;
	
	public int scaleLevel = 0;
	public int scaleCooldown = 0;

	public final static double gravitation = 0.008d, maxSpeed = 1d,
			accuracy = 0.005d;

	public final int blockPixelHeight = 16;
	
	public GameScreen gameScreen;
	public GameManager gameManager;
	

	protected Level(GameManager gameManager, GameScreen gameScreen, int sizeX, int sizeY,
			int sizeFactor, BufferedImage background) {
		this.gameManager = gameManager;
		this.gameScreen = gameScreen;
		this.size = new Rectangle(sizeX, sizeY);
		this.entities = new LevelEntities(size);
		this.background = background;
		this.canvas = new Canvas(this,sizeFactor, new Point((int)sizeX/2,(int)sizeY/2), new Dimension(800,600));
	}

	protected void addEntity(Entity entity2add) {
		if (entity2add != null) {
			entity2add.setLevel(this);
			this.entities.register(entity2add);
		}
	}

	protected void setPlayer(Player player) {
		this.player = player;
		this.addEntity(player);
		canvas.trackObject(this.player);
	}

	public void doTick() {
		/* resizing Part */
		if(scaleCooldown > 0) {
			scaleCooldown--;
		} else {
			if(this.gameManager.keyboard.isKeyPressed(Keyboard.F1)) {
				this.canvas.decreaseScaleLevel();		
				scaleCooldown=25;
			} else if(this.gameManager.keyboard.isKeyPressed(Keyboard.F2)) {
				this.canvas.increaseScaleLevel();		
				scaleCooldown=25;
			}
		}		
		entities.doTick();
	}
	
	public LeverSystem getLeverSystem(int colorID) {
		if(colorID >= this.leverSystems.length) {
			System.err.printf("[Level] Did not find LeverSystem %i.%n", colorID);
			colorID = 0;
		}
		
		if(leverSystems[colorID] == null) {
			leverSystems[colorID] = new LeverSystem(colorID);
		}
		
		return leverSystems[colorID];
	}
	
	public BufferedImage render(ScreenSize screenSize) {
		return this.canvas.render(screenSize);
	}
	
	public void finish() {
		this.gameScreen.finischedLevel();
	}
	
	public void playerDied() {
		this.gameScreen.reloadLevel();
	}
}