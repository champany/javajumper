package de.champany.javajumper.level.entitystoragesystem;

import java.awt.geom.*;
import java.util.Vector;
import de.champany.javajumper.level.entity.Entity;

public class LevelEntities {
	//public Vector<EntityChunk> entityChunks;
	public Vector<Entity> entities = new Vector<Entity>();
	//public final static int chunkSize = 10;
	
	//public EntityChunkGroup(Rectangle2D size) {
	public LevelEntities(Rectangle2D size) {
	/*int countX = (int)Math.ceil(size.getWidth()/chunkSize);
		int countY = (int)Math.ceil(size.getHeight()/chunkSize);
		
		entities = new Vector<Entity>();
		entityChunks = new Vector<EntityChunk>();
		
		for(int x = 0; x < countX; x++) {
			for(int y = 0; y < countY; y++) {
				this.entityChunks.add(new EntityChunk(
						new Rectangle2D.Double(x*chunkSize,y*chunkSize,chunkSize,chunkSize)));
			}
		}*/
	}
	
	public void doTick() {
		for (Entity e : this.entities) {
			if(e instanceof TickAble) e.doTick();
		}

	}
	
	public void register(Entity entity) {
		if(entity!=null) {
			this.addEntity(entity);
		}
		
		/*for (EntityChunk ec : this.entityChunks) {
			ec.register(entity);
		}*/

	}
	
	/*public void checkCollision(Rectangle2D collision, CollisionAble toBeNotified, Object self) {
		/*for (EntityChunk ec : this.entityChunks) {
			ec.checkCollision(collision, toBeNotified, self);
		}
		ec.checkCollision(collision, toBeNotified, self);
	}*/

	public void checkCollision(Rectangle2D collision, CollisionAble toBeNotified, Object self) {
			for (Entity entity : this.entities) {
				if ((entity != null) && (entity!=self)) {
					toBeNotified.collidedBy(entity);
				}
			}
		}

	
	public void addEntity(Entity entity) {
		if (entity != null) {
			if (!entities.contains(entity))
				this.entities.add(entity);
		}
	}

}
