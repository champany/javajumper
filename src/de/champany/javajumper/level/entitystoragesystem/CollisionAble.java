package de.champany.javajumper.level.entitystoragesystem;

import de.champany.javajumper.level.entity.Entity;

public interface CollisionAble {
	public abstract void collidedBy(Entity e);
}
