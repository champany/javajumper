package de.champany.javajumper.level.entitystoragesystem;

import java.awt.geom.Rectangle2D;
import java.util.Vector;

import de.champany.javajumper.level.entity.Entity;

public class EntityChunk {
	public Rectangle2D collision;
	public Vector<Entity> entities;
	
	public EntityChunk(Rectangle2D collision) {
		this.collision = collision;
		this.entities = new Vector<Entity>();
	}
	
	public void register(Entity entity) {
		if(entity!=null) {
			if(entity.sprite.intersects(collision)) {
				this.addEntity(entity);
			} else {
				this.removeEntity(entity);
			}
		}
	}
	
	public void update() {
		
	}
	
	public void checkCollision(Rectangle2D collision, CollisionAble toBeNotified, Object self) {
		Rectangle2D collisionLarge = 
				new Rectangle2D.Double(collision.getX()-1, collision.getY()-1, collision.getWidth()+2, collision.getHeight()+2);

		if (this.collision.intersects(collisionLarge)) {
			for (Entity entity : this.entities) {
				if ((entity != null) && (entity!=self) && this.collision.intersects(entity.collision)) {
					toBeNotified.collidedBy(entity);
				}
			}
		}
	}
	
	public void addEntity(Entity entity) {
		if (entity != null) {
			this.removeEntity(entity);
			this.entities.add(entity);
		}
	}
	
	public void removeEntity(Entity entity) {
		if (entities.contains(entity)) {
			this.entities.remove(entity);
		}
	}
	
}
