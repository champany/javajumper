package de.champany.javajumper.screen;

import java.awt.Graphics2D;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.GameManager;
import de.champany.javajumper.level.Level;
import de.champany.lib.Keyboard;

public class GameScreen extends Screen {
	Level level;
	GameManager gm;
	Screen screenOnTop;
	
	int levelID;
	
	public GameScreen(GameManager gm) {
		this.gm = gm;
		
		this.loadLevel(0);
	}
	
	public void loadLevel(int levelID) {
		this.level = Level.loadFromImage(gm, this,
				ArtsLoader.levels.get(levelID), 25,
				ArtsLoader.background);
	}
	
	public void loadNextLevel() {
		levelID++;
		if(levelID >= ArtsLoader.levels.size()) {
			levelID = 0;
		}
		this.loadLevel(levelID);
	}
	
	public void setScreenOnTop(Screen newScreen) {
		this.screenOnTop = newScreen;
	}
	
	public void doTick() {
		if(this.screenOnTop != null) this.screenOnTop.doTick();
		if(this.level!=null)level.doTick();
		if(this.gm.keyboard.isKeyPressed(Keyboard.F4)) this.reloadLevel();
		//if(this.gm.keyboard.isKeyTyped(Keyboard.A)) System.out.println("typed");
	}

	public void paint(Graphics2D g, ScreenSize screenSize) {
		if(this.level != null) g.drawImage(level.render(screenSize),0,0,null);
	}
	
	public void finischedLevel() {
		this.level = null;
		this.loadNextLevel();
	}
	
	public void reloadLevel() {
		this.loadLevel(this.levelID);
	}
}
