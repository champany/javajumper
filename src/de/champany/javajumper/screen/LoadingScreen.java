package de.champany.javajumper.screen;

import java.awt.Color;
import java.awt.Graphics2D;

import de.champany.javajumper.ArtsLoader;
import de.champany.javajumper.GameManager;

public class LoadingScreen extends Screen {
	Screen screenOnTop;
	GameManager gm;
	
	public LoadingScreen(GameManager gm) {
		this.gm = gm;
	}
	
	public void doTick() {
		if(this.screenOnTop != null) this.screenOnTop.doTick();
		if(this.screenOnTop == null) {
			screenOnTop = new GameScreen(gm);
		}
	}
	
	public void setScreenOnTop(Screen newScreen) {
		this.screenOnTop = newScreen;
	}
	
	public void paint(Graphics2D g, ScreenSize screenSize) {
		if(screenOnTop==null) {
			g.setColor(new Color(250,200,0));
			g.fillRect(0, 0, screenSize.getWidth(), screenSize.getHeight());
			g.drawImage(ArtsLoader.loading,(screenSize.getWidth()-ArtsLoader.loading.getWidth())/2,(screenSize.getHeight()-ArtsLoader.loading.getHeight())/2,ArtsLoader.loading.getWidth(),ArtsLoader.loading.getHeight(), null);
		} else {
			screenOnTop.paint(g, screenSize);
		}
	}
}
