package de.champany.javajumper.screen;

import java.awt.Graphics2D;

public abstract class Screen {	
	public abstract void doTick();
	
	public abstract void paint(Graphics2D g, ScreenSize screenSize);
}