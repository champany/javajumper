package de.champany.javajumper.screen;

public class ScreenSize {
	private int width=0, height=0;
	
	public ScreenSize(int width, int height) {
		this.setSize(width, height);
	}
	
	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setSize(int width, int height) {
		this.width=(int) Math.max(0, width);
		this.height=(int) Math.max(0, height);		
	}
}
