package de.champany.javajumper;

public final class Logger {
	public static Logger errorlogger;
	public double highscore;
	public static Logger getInstance() {
		if(errorlogger == null) {
			errorlogger = new Logger();
		}
		return errorlogger;
	}
	
	public void setHighscore(double highscore) {
		this.highscore = Math.max(this.highscore, highscore);
	}
	
	public double getHighscore() {
		return highscore;
	}

}
