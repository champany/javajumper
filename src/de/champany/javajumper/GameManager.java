/*package de.champany.javajumper;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import de.champany.javajumper.level.Level;
import de.champany.javajumper.screen.LoadingScreen;
import de.champany.javajumper.screen.Screen;
import de.champany.javajumper.screen.ScreenSize;
import de.champany.lib.Keyboard;
import de.champany.lib.CTickTimer;

public class GameManager extends JPanel {
	private static final long serialVersionUID = 0L;

	public final static int FPS = 60;
	public static CTickTimer tickTimer = new CTickTimer(FPS); 
	public int tick = 0;

	public ArtsLoader res;
	public Level level;
	public JavaJumper jjumper;
	public Keyboard keyboard;

	public long ticksPerSecond = 60;
	public long lastNanoTime = 0;
	public long calculatedNanoTime = 0;
	public long nanoTime = 0;
	public int levelID = 0;
	
	public Screen screen;
	public ScreenSize screensize;

	public final static int KEY_UP = 1;
	public final static int KEY_LEFT = 2;
	public final static int KEY_DOWN = 3;
	public final static int KEY_RIGHT = 4;
	public final static int KEY_RESTART = 5;
	public static final int KEY_DEBUG = 6;
	public static final int KEY_CHEAT = 7;

	public GameManager(JavaJumper jjumper, Keyboard keyboard) {
		this.jjumper = jjumper;
		this.res = new ArtsLoader();
		this.keyboard = keyboard;
		this.screen = new LoadingScreen(this);
		this.screensize = new ScreenSize(800,600);
	}

	@Override
	public void paintComponent(Graphics gNot2D) {
		super.paintComponent(gNot2D);
		Graphics2D g = (Graphics2D) gNot2D;
		this.renderScreen(g);
		g.dispose();
	}

	public void renderScreen(Graphics2D g) {
		this.screensize = new ScreenSize(this.getWidth(),this.getHeight());
		this.screen.paint(g, this.screensize);
	}

	public void start() {
		this.calculatedNanoTime = System.nanoTime();
		this.screensize = new ScreenSize(this.getWidth(),this.getHeight());
		
		Timer t = new Timer(1000 / GameManager.FPS, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GameManager.tickTimer.newTick();
				while (GameManager.tickTimer.hasToTick()) {
					GameManager.tickTimer.tick();
					screen.doTick();
				}

				keyboard.doTick();

				GameManager.this.repaint();
				GameManager.tickTimer.endTick();
				
				//System.out.println("Highscore: "+(int)Logger.getInstance().getHighscore());
				//System.out.println(GameManager.tickTimer.getAverage()/(1000*1000));
			}
		});
		t.start();
	}
}*/

package de.champany.javajumper;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import de.champany.javajumper.level.Level;
import de.champany.javajumper.screen.LoadingScreen;
import de.champany.javajumper.screen.Screen;
import de.champany.javajumper.screen.ScreenSize;
import de.champany.lib.Keyboard;
import de.champany.lib.CTickTimer;

public class GameManager extends JPanel {
	private static final long serialVersionUID = 0L;

	public final static int FPS = 60;
	public static CTickTimer tickTimer = new CTickTimer(); 
	public int tick = 0;

	public ArtsLoader res;
	public Level level;
	public JavaJumper jjumper;
	public Keyboard keyboard;

	public long ticksPerSecond = 60;
	public long lastNanoTime = 0;
	public long calculatedNanoTime = 0;
	public long nanoTime = 0;
	public int levelID = 0;
	
	public Screen screen;
	public ScreenSize screensize;

	public final static int KEY_UP = 1;
	public final static int KEY_LEFT = 2;
	public final static int KEY_DOWN = 3;
	public final static int KEY_RIGHT = 4;
	public final static int KEY_RESTART = 5;
	public static final int KEY_DEBUG = 6;
	public static final int KEY_CHEAT = 7;

	public GameManager(JavaJumper jjumper, Keyboard keyboard) {
		this.jjumper = jjumper;
		this.res = new ArtsLoader();
		this.keyboard = keyboard;
		this.screen = new LoadingScreen(this);
		this.screensize = new ScreenSize(800,600);
	}

	@Override
	public void paintComponent(Graphics gNot2D) {
		super.paintComponent(gNot2D);
		Graphics2D g = (Graphics2D) gNot2D;
		this.renderScreen(g);
		g.dispose();
	}

	public void renderScreen(Graphics2D g) {
		this.screensize = new ScreenSize(this.getWidth(),this.getHeight());
		this.screen.paint(g, this.screensize);
	}

	public void start() {
		this.calculatedNanoTime = System.nanoTime();
		this.screensize = new ScreenSize(this.getWidth(),this.getHeight());
		Timer t = new Timer(1000 / GameManager.FPS, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GameManager.tickTimer.tickBegin();
				GameManager.this.nanoTime = System.nanoTime();
				while (GameManager.this.calculatedNanoTime < GameManager.this.nanoTime) {
					GameManager.this.calculatedNanoTime += (1000 / GameManager.this.ticksPerSecond) * 1000000;
					screen.doTick();
				}

				keyboard.doTick();

				GameManager.this.repaint();
				GameManager.this.lastNanoTime = GameManager.this.nanoTime;
				GameManager.this.tick++;
				GameManager.tickTimer.tickEnd();
				
				//System.out.println("Highscore: "+(int)Logger.getInstance().getHighscore());
				//System.out.println(GameManager.tickTimer.getAverage()/(1000*1000));
			}
		});
		t.start();
	}
}