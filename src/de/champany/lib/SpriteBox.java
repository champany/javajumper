package de.champany.lib;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

public class SpriteBox extends Box {
	private static final long serialVersionUID = 1L;
	public BufferedImage image;
	public boolean flipVertical;
	public BufferedImage flippedImage;

	public SpriteBox(BufferedImage image, Point2D.Double point,
			double paddingUp, double paddingRight, double paddingDown,
			double paddingLeft) {
		if (point != null) {
			this.point = point;
		} else {
			this.point = new Point2D.Double(0d, 0d);
		}
		this.image = image;
		this.padding[Box.sUP] = paddingUp;
		this.padding[Box.sRIGHT] = paddingRight;
		this.padding[Box.sDOWN] = paddingDown;
		this.padding[Box.sLEFT] = paddingLeft;
	}

	public BufferedImage getImage() {
		if (!flipVertical) {
			return image;
		} else {
			if (flippedImage == null) {
				createFlippedImage();
			}
			return flippedImage;
		}
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
		this.flippedImage = null;
	}

	//TODO Integriere in Zeichenfunktion etc.
	public void createFlippedImage() {
		flippedImage = new BufferedImage(image.getWidth(), image.getHeight(),
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) flippedImage.getGraphics();
		g.drawImage(image, image.getWidth(), 0, -image.getWidth(),
				image.getHeight(), null);
		g.dispose();
	}
}
