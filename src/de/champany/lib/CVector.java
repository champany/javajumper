package de.champany.lib;

public class CVector {
	double x, y;
	
	public CVector(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public CVector() {
		this(0,0);
	}
	
	public void add(CVector vector) {
		this.x = this.x + vector.x;
		this.y = this.y + vector.y;
	}
	
	public void factor(double multiplier) {
		this.x *= multiplier;
		this.y *= multiplier;
	}
	
	public void subtract(CVector vector) {
		this.x = this.x - vector.x;
		this.y = this.y - vector.y;
	}
	
	public double getLength() {
		return Math.sqrt(this.x*this.x+this.y*this.y);
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
}
