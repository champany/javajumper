package de.champany.lib;

public class CMath {
	public static long min(long a, long b) {
		return (a>b)?a:b;
	}
	
	public static long max(long a, long b) {
		return (a<b)?a:b;
	}
	
	public static int min(int a, int b) {
		return (a>b)?a:b;
	}
	
	public static int max(int a, int b) {
		return (a<b)?a:b;
	}

	
	public static double min(double a, double b) {
		return (a>b)?a:b;
	}
	
	public static double max(double a, double b) {
		return (a<b)?a:b;
	}
	
	public static double getInRange(double x, double min, double max) {
		if(x > max) {
			return max;
		} else if (x < min) {
			return min;
		} else {
			return x;
		}
	}
	
	public static double getInRangeByCutting(double x, double max) {
		return CMath.getInRange(x, -max, max);
	}
	
	public static double getInRangeByAdding(double x, double min, double max) {
		while(x > max) {
			x-= (max-min);
		}
		while(x < min) {
			x+= (max-min);
		}
		return x;
	}
	
	public static int getInRangeByAdding(int x, int min, int max) {
		while(x > max) {
			x-= (max-min);
		}
		while(x < min) {
			x+= (max-min);
		}
		return x;
	}

	
	public static int cCeil(double x) {
		int vorzeichen = (x<0)?-1:1;
		return (int) (Math.ceil(Math.abs(x))*vorzeichen);
	}
	public static int cFloor(double x) {
		int vorzeichen = (x<0)?-1:1;
		return (int) (Math.floor(Math.abs(x))*vorzeichen);
	}

}
