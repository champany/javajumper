package de.champany.lib;

import java.awt.image.BufferedImage;

public class SpriteMap {
	// TODO testing
	public static SpriteMap loadAndSplitIntoSquaresbyAmountY(String file,
			int amountY) {
		BufferedImage tmpImg = CTools.loadImage(file);
		int width = (int) Math.floor(tmpImg.getWidth() / amountY);
		int height = width;
		if ((height <= 0) || (width <= 0)) {
			System.err
					.printf("[SpriteMap] Splitting image failed, because %s == %d while amountY == %d and image-size == (%d,%d)",
							(Math.min(width, height) == width) ? "width"
									: "height", Math.min(width, height),
							amountY, tmpImg.getWidth(), tmpImg.getHeight());
		}
		return SpriteMap.splitBySize(tmpImg, width, height);
	}

	public static SpriteMap splitBySize(BufferedImage source,
			int widthOfSprite, int heightOfSprite) {
		if (source != null) {
			int amountX = (int) Math.floor(source.getWidth() / widthOfSprite);
			int amountY = (int) Math.floor(source.getHeight() / heightOfSprite);
			if ((amountY == 0) || (amountX == 0)) {
				return null;
			}

			BufferedImage[] output = new BufferedImage[amountX * amountY];
			for (int currentX = 0; currentX < amountX; currentX++) {
				for (int currentY = 0; currentY < amountY; currentY++) {
					output[currentX + (currentY * amountX)] = source
							.getSubimage(currentX * widthOfSprite, currentY
									* heightOfSprite, widthOfSprite,
									heightOfSprite);
				}
			}
			return new SpriteMap(output, amountX, amountY);

		} else {
			return null;
		}
	}
	
	public static SpriteMap splitIntoSquaresByAmountY(BufferedImage source,
			int amountY) {
		if (source != null) {
			int sizeOfSprite = Math.floorDiv(source.getHeight(), amountY);
			int amountX = (int) Math.floor(source.getWidth() / sizeOfSprite);
			if ((amountY == 0) || (amountX == 0)) {
				return null;
			}

			BufferedImage[] output = new BufferedImage[amountX * amountY];
			for (int currentX = 0; currentX < amountX; currentX++) {
				for (int currentY = 0; currentY < amountY; currentY++) {
					output[currentX + (currentY * amountX)] = source
							.getSubimage(currentX * sizeOfSprite, currentY
									* sizeOfSprite, sizeOfSprite,
									sizeOfSprite);
				}
			}
			return new SpriteMap(output, amountX, amountY);

		} else {
			return null;
		}
	}

	private BufferedImage[] sprites;

	private int width, height;

	public int length;

	public SpriteMap(BufferedImage[] sprites, int amountX, int amountY) {
		this.sprites = sprites;
		this.width = amountX - 1;
		this.height = amountY - 1;
		this.length = sprites.length;
	}

	public BufferedImage getSprite() {
		return this.getSprite(0);
	}

	public BufferedImage getSprite(int id) {
		if ((id >= this.sprites.length) | (id < 0)) {
			System.err.printf("[SpriteMap] Sprite #%d wasn't not found.%n", id);
			return this.onFail();
		} else {
			return this.sprites[id];
		}
	}

	public BufferedImage getSprite(int x, int y) {
		if ((x > this.width) || (y > this.height)) {
			System.err
					.printf("[SpriteMap] Given coords (%d|%d) aren't in the field (%d|%d)%nAnyway, I'll try to give back another imgage.%n",
							x, y, this.width, this.width);
		}
		int id = (this.width * y) + x;
		BufferedImage tmp = this.getSprite(id);
		if (tmp == null) {
			System.err.printf("The coords were (%d,%d)", x, y);
			return this.onFail();
		}
		return tmp;
	}

	private BufferedImage onFail() {
		if (this.sprites.length >= 1) {
			return this.getSprite(0);
		} else {
			return null;
		}
	}
}
