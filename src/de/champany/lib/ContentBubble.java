package de.champany.lib;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class ContentBubble {
	public final static Integer UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3;
	public final static Integer UPLEFT = 0, UPRIGHT = 1, DOWNLEFT = 2, DOWNRIGHT = 3;	
	public BufferedImage[] corner;
	public BufferedImage[] mid;
	public BufferedImage center;
	public BufferedImage bubbleConnection;
	public BufferedImage bubbleFoot;
	public int scale = -1;
	
	public ContentBubble(BufferedImage source) {
		loadImage(source);
	}
	
	public void loadImage(BufferedImage source) {
		scale = (int) Math.floor(Math.min(source.getHeight()/5d,source.getWidth()/4d));
		corner = new BufferedImage[4];
		corner[UPLEFT] = source.getSubimage(0*scale, 0*scale, scale, scale);
		corner[DOWNLEFT] = source.getSubimage(0*scale, 2*scale, scale, scale);
		corner[UPRIGHT] = source.getSubimage(2*scale, 0*scale, scale, scale);
		corner[DOWNRIGHT] = source.getSubimage(2*scale, 2*scale, scale, scale);
		mid = new BufferedImage[4];
		mid[UP] = source.getSubimage(1*scale, 0*scale, scale, scale);
		mid[LEFT] = source.getSubimage(0*scale, 1*scale, scale, scale);
		mid[RIGHT] = source.getSubimage(2*scale, 1*scale, scale, scale);
		mid[DOWN] = source.getSubimage(1*scale, 2*scale, scale, scale);
		center = source.getSubimage(1*scale, 1*scale, scale, scale);
	}
	
	public void createFrame(int width, int height) {
		BufferedImage frame = new BufferedImage(width+2*scale,height+2*scale,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) frame.getGraphics();
		g.drawImage(corner[UPLEFT], 0, 0, scale, scale, null);
		g.drawImage(corner[DOWNLEFT], 0, 0, scale, scale, null);
		g.drawImage(corner[UPLEFT], 0, 0, scale, scale, null);
		g.drawImage(corner[UPLEFT], 0, 0, scale, scale, null);

	}
}