package de.champany.lib;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class Box extends Rectangle2D.Double {
	private static final long serialVersionUID = 1L;
	public Point2D.Double point;
	public double[] padding = new double[4];

	public static final int sUP = 0, sRIGHT = 1, sDOWN = 2, sLEFT = 3;

	public Box() {
		this.setRect(0, 0, 1, 1);
	}

	public Box(double x, double y, double w, double h) {
		this.setRect(x, y, w, h);
	}

	public Box(Point2D.Double point, double paddingUp, double paddingRight,
			double paddingDown, double paddingLeft) {
		if (point != null) {
			this.point = point;
		} else {
			this.point = new Point2D.Double(0d, 0d);
		}
		this.padding[Box.sUP] = paddingUp;
		this.padding[Box.sRIGHT] = paddingRight;
		this.padding[Box.sDOWN] = paddingDown;
		this.padding[Box.sLEFT] = paddingLeft;
	}

	public void extendRight(int i) {
		this.padding[Box.sRIGHT] += i;
	}
	
	@Override
	public double getHeight() {
		return this.padding[Box.sUP] + this.padding[Box.sDOWN];
	}

	@Override
	public double getWidth() {
		return this.padding[Box.sLEFT] + this.padding[Box.sRIGHT];
	}

	@Override
	public double getX() {
		return this.point.getX() - this.padding[Box.sLEFT];
	}

	@Override
	public double getY() {
		return this.point.getY() - this.padding[Box.sUP];
	}

	@Override
	public boolean isEmpty() {
		return (this.getWidth() == 0) || (this.getHeight() == 0);
	}

	public void setPadding(double paddingUp, double paddingRight,
			double paddingDown, double paddingLeft) {
		this.padding[Box.sUP] = paddingUp;
		this.padding[Box.sRIGHT] = paddingRight;
		this.padding[Box.sDOWN] = paddingDown;
		this.padding[Box.sLEFT] = paddingLeft;
	}

	public void setPoint(Point2D.Double point) {
		this.point = new Point2D.Double(point.getX(), point.getY());
	}

	@Override
	public void setRect(double x, double y, double w, double h) {
		this.setPadding(0, x, y, 0);
		this.setPoint(new Point2D.Double(x, y));
	}
}