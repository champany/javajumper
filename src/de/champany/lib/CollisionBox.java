package de.champany.lib;

import java.awt.geom.Point2D;

public class CollisionBox extends Box {
	private static final long serialVersionUID = 1L;
	public Point2D.Double tmpPoint = new Point2D.Double();
	public boolean tmpMode;

	public CollisionBox() {
		super(0, 0, 1, 1);
	}

	public CollisionBox(double x, double y, double w, double h) {
		super(x, y, w, h);
	}

	public CollisionBox(Point2D.Double point, double paddingUp,
			double paddingRight, double paddingDown, double paddingLeft) {
		super(point, paddingUp, paddingRight, paddingDown, paddingLeft);
	}

	public int getDirectionOf(CollisionBox col) {
		// TODO implement
		return 0;
	}

	@Override
	public double getX() {
		return (tmpMode ? this.tmpPoint.getX() : this.point.getX())
				- this.padding[Box.sLEFT];
	}

	@Override
	public double getY() {
		return (tmpMode ? this.tmpPoint.getY() : this.point.getY())
				- this.padding[Box.sUP];
	}
	
	public void doTmpMovement(double x, double y) {
		tmpPoint.setLocation(this.point.x + x, this.point.y + y);
		tmpMode = true;
	}

	public void applyTmpMovement() {
		if (tmpMode) {
			point.setLocation(tmpPoint.x, tmpPoint.y);
		}
		tmpMode = false;
		// this.improvePointDoubles();
	}

	public void discardTmpMovement() {
		tmpMode = false;
	}
	
}