package de.champany.lib;

public class CTickTimer {
	protected double tickTimeAvg;
	protected long tickCount;
	protected long lastTick;
	protected boolean tickStarted;
	
	public CTickTimer() {
		
	}
	
	public void tickBegin() {
		this.lastTick = System.nanoTime();
		tickStarted = true;
	}
	
	public void tickEnd() {
		if(tickStarted) {
			//tickTimeAvg = ((tickTimeAvg * tickCount) + (System.nanoTime() - lastTick)) / (tickCount + 1);
			//tickTimeAvg = ((tickTimeAvg * tickCount) + (System.nanoTime() - lastTick)) / (tickCount + 1);
			tickTimeAvg = ((tickTimeAvg)*3 + (System.nanoTime() - lastTick))/4;
			tickCount++;
			tickStarted = false;
		}
	}
	
	public double getAverage() {
		return tickTimeAvg;
		
	}
}