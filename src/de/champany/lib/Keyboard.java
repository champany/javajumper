package de.champany.lib;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {
	private int keys[] = new int[3];

	public static final int UP    = Integer.parseInt("00000000000001", 2);//0b0000000000001;
	public static final int DOWN  = Integer.parseInt("00000000000010", 2);//0b0000000000010;
	public static final int LEFT  = Integer.parseInt("00000000000100", 2);//000000000100;
	public static final int RIGHT = Integer.parseInt("00000000001000", 2);//0b0000000001000;
	public static final int W     = Integer.parseInt("00000000010000", 2);//0b0000000010000;
	public static final int A     = Integer.parseInt("00000000100000", 2);//0b0000000100000;
	public static final int S     = Integer.parseInt("00000001000000", 2);//0b0000001000000;
	public static final int D     = Integer.parseInt("00000010000000", 2);//0b0000010000000;
	public static final int SPACE = Integer.parseInt("00000100000000", 2);//0b0000100000000;
	public static final int F1    = Integer.parseInt("00001000000000", 2);//0b0001000000000;
	public static final int F2    = Integer.parseInt("00010000000000", 2);//0b0010000000000;
	public static final int F3    = Integer.parseInt("00100000000000", 2);//0b0100000000000;
	public static final int F4    = Integer.parseInt("01000000000000", 2);//0b1000000000000;
	public static final int SHIFT = Integer.parseInt("10000000000000", 2);//0b10000000000000;

	public static final int UNUSED = -1;

	public static final int MOVE_JUMP = Keyboard.UP | Keyboard.W
			| Keyboard.SPACE;
	public static final int MOVE_LEFT = Keyboard.LEFT | Keyboard.A;
	public static final int MOVE_RIGHT = Keyboard.RIGHT | Keyboard.D;
	public static final int MOVE_USE = Keyboard.DOWN | Keyboard.S | Keyboard.SHIFT;
	public static final int DO_RELOAD = Keyboard.F3;

	public static final int CURRENT = 0;
	public static final int LAST = 1;
	public static final int TYPED = 2;

	public Keyboard() {
		this.releaseAllKeys();
		this.keys[Keyboard.LAST] = 0;
		this.keys[Keyboard.TYPED] = 0;
	}

	public void doTick() {
		this.keys[Keyboard.LAST] = this.keys[Keyboard.CURRENT];
		this.keys[Keyboard.TYPED] = (this.keys[Keyboard.CURRENT] ^ this.keys[Keyboard.LAST])
				& this.keys[Keyboard.CURRENT];
	}

	public boolean isKeyPressed(int keyID) {
		if (keyID == Keyboard.UNUSED) {
			return false;
		}

		return (this.keys[Keyboard.CURRENT] & keyID) != 0;
	}

	public boolean isKeyTyped(int keyID) {
		if (keyID == Keyboard.UNUSED) {
			return false;
		}
		return ((this.keys[Keyboard.TYPED] & keyID) != 0) ? true : false;
	}

	private int key2id(int keyCode) {
		switch (keyCode) {
		case KeyEvent.VK_UP:
			return Keyboard.UP;
		case KeyEvent.VK_DOWN:
			return Keyboard.DOWN;
		case KeyEvent.VK_LEFT:
			return Keyboard.LEFT;
		case KeyEvent.VK_RIGHT:
			return Keyboard.RIGHT;
		case KeyEvent.VK_W:
			return Keyboard.W;
		case KeyEvent.VK_A:
			return Keyboard.A;
		case KeyEvent.VK_S:
			return Keyboard.S;
		case KeyEvent.VK_D:
			return Keyboard.D;
		case KeyEvent.VK_SPACE:
			return Keyboard.SPACE;
		case KeyEvent.VK_F1:
			return Keyboard.F1;
		case KeyEvent.VK_F2:
			return Keyboard.F2;
		case KeyEvent.VK_F3:
			return Keyboard.F3;
		case KeyEvent.VK_F4:
			return Keyboard.F4;
		case KeyEvent.VK_SHIFT:
			return Keyboard.SHIFT;
		default:
			return -1;
		}
	}

	@Override
	public void keyPressed(KeyEvent keyCode) {
		this.setBits(this.key2id(keyCode.getKeyCode()));
	}

	@Override
	public void keyReleased(KeyEvent keyCode) {
		this.unsetBit(this.key2id(keyCode.getKeyCode()));
	}


	public void releaseAllKeys() {
		this.keys[Keyboard.CURRENT] = 0;
	}

	private void setBits(int bit) {
		if (bit == Keyboard.UNUSED) {
			return;
		}
		this.keys[Keyboard.CURRENT] |= bit;
	}

	private void unsetBit(int bit) {
		// TODO think about problem, that if bit, more bits could be interfeared
		if (bit == Keyboard.UNUSED) {
			return;
		}
		if (isKeyPressed(bit)) {
			keys[0] -= bit;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		//System.out.println("Event");
	}
}
