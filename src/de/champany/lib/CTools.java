package de.champany.lib;

import java.awt.Font;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

public class CTools {
	
	public static Font loadFont(String path, int size) {
		Font fontBase = null;
		Font realFont = null;
		try {
			fontBase = Font.createFont(Font.TRUETYPE_FONT, CTools.class.getResourceAsStream(path));
			realFont = fontBase.deriveFont(Font.PLAIN, size);
		} catch (Exception ex) {
			System.err.println("Pfad: "+path);
		}
		return realFont;
	}
	
	public static BufferedImage loadImage(String src) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(SpriteMap.class.getResourceAsStream(src));
		} catch (Exception ex) {
			System.err.printf("[SpiteMap] Failed loading Image %s.%n", src);
			return null;
		}
		return img;
	}
}
